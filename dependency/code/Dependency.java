package dependency.code;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.ansj.domain.Term;
import org.ansj.splitWord.analysis.ToAnalysis;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.trees.TypedDependency;
import edu.stanford.nlp.trees.international.pennchinese.ChineseTreebankLanguagePack;

public class Dependency extends _Keyven.StanfordParser {
	public Collection<TypedDependency> tdls = null;

	public DepObject process(String str, boolean logprint) {
		DepObject obj = new DepObject();

		tdls = dependencyTree(str);

		TypedDependency s = null, p = null, o = null;
		for (TypedDependency v : tdls) {
			if (logprint == true) {
				System.out.println(Dependency.log(v));
			}
			if (v.reln().getShortName().equals("root")) {
				obj.setPredicate(v.dep().pennString());
				p = v;
			}
			if (v.reln().getShortName().matches("nsubj|top") && s == null) {
				obj.setSubject(v.dep().pennString());
				s = v;
			}
			if (v.reln().getShortName().matches("nsubjpass|dobj|attr")) {
				obj.setObject(v.dep().pennString());
				o = v;
			}
		}
		if (obj.getObject().length() == 0) {
			for (TypedDependency v : tdls) {
				if (v.reln().getShortName().matches("cop")) {
					obj.setObject(obj.getPredicate());
					o = p;
					obj.setPredicate(v.dep().pennString());
					p = v;
				} else if (v.reln().getShortName().matches("nsubj")) {
					if (!v.dep().pennString().trim().equals(obj.getSubject())) {
						obj.setObject(v.dep().pennString());
						o = v;
					}
				}
			}
		}
		for (TypedDependency v : tdls) {
			if (v.reln().getShortName().matches("ccomp")) {
				if (obj.getObject().length() == 0) {
					obj.setPredicate(v.dep().pennString());
					p = v;
				}
			}
		}

		if (s == null || p == null || o == null) {
			obj.setObject("");
			obj.setPredicate("");
			obj.setSubject("");
		}

		return obj;
	}

	public static String log(TypedDependency v) {
		return v.reln().getShortName() + "[dep:" + v.dep().pennString().trim()
				+ ",gov:" + v.gov().pennString().trim() + "]";
	}

	public static String repair(String str, String ans) {
		if (str.equals(ans)) {
			return ans;
		}
		Collection<TypedDependency> ptdls = dependencyTree(str);

		for (TypedDependency v : ptdls) {
			if (v.reln().getShortName().matches("nn")) {
				if (str.indexOf(v.gov().pennString().trim()) >= 0) {
					ans = ans.replace(v.gov().pennString().trim(), v.dep()
							.pennString().trim()
							+ v.gov().pennString().trim());
				}
			}
			if (v.reln().getShortName().matches("mark|case")) {
				if (v.dep().pennString().trim().equals("��")) {
					if (ans.indexOf(v.gov().pennString().trim()) >= 0) {
						ans = ans.replace(v.gov().pennString().trim(), v.gov()
								.pennString().trim()
								+ v.dep().pennString().trim());
					}
				}
			}
		}

		return ans;
	}

	public static boolean isCC(DepObject obj, String str) {
		Collection<TypedDependency> ptdls = dependencyTree(str);

		for (TypedDependency v : ptdls) {
			if (v.reln().getShortName().matches("cc")) {
				if (v.gov().pennString().trim().equals(obj.getObject())) {
					return true;
				}
			}
		}

		return false;
	}

	public static Collection<TypedDependency> dependencyTree(String str) {
		List<Term> parse1 = ToAnalysis.parse(str);
		List<TaggedWord> list = new ArrayList<TaggedWord>();
		for (int i = 0; i < parse1.size(); i++) {
			list.add(new TaggedWord(parse1.get(i).getName().toString(), null));
		}

		Tree ptree = (Tree) lp.parse(list);
		TreebankLanguagePack tlp = new ChineseTreebankLanguagePack();
		GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
		GrammaticalStructure gs = gsf.newGrammaticalStructure(ptree);
		Collection<TypedDependency> ptdls = gs
				.typedDependenciesCCprocessed(true);
		return ptdls;
	}
}
